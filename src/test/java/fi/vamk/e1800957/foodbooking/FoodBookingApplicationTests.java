package fi.vamk.e1800957.foodbooking;

import static org.assertj.core.api.Assertions.assertThat;

import org.apache.commons.collections4.IterableUtils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import fi.vamk.e1800957.foodbooking.DTO.BookingDTO;
import fi.vamk.e1800957.foodbooking.DTO.ItemDTO;
import fi.vamk.e1800957.foodbooking.Entity.Booking;
import fi.vamk.e1800957.foodbooking.Entity.Item;
import fi.vamk.e1800957.foodbooking.Repository.BookingRepository;
import fi.vamk.e1800957.foodbooking.Repository.ItemRepository;



@SpringBootTest
@AutoConfigureMockMvc
class FoodBookingApplicationTests extends AbstractTest{
	
	@Autowired
	private ItemRepository itemRepository;
	
	@Autowired
	private BookingRepository bookingRepository;
	
	@Autowired
    private MockMvc mvc;

	@Test
	//Testcase 1, the application should save, fetch by id and delete the item
	void testCaseSaveItem() {
		Iterable<Item> begin= itemRepository.findAll();
		Item i1= new Item("abc", "abc", 10, "cc", "adsds");
		Item saved= itemRepository.save(i1);
		Item found= itemRepository.findById(i1.getId()).get();
		assertThat(found.toString()).isEqualTo(i1.toString());
		itemRepository.delete(found);
		Iterable<Item> end= itemRepository.findAll();
		assertEquals((long) IterableUtils.size(begin), (long) IterableUtils.size(end));
		
	}
	
	@Test
	//Testcase 2, the application should save, fetch by id and delete the booking
	void testCaseSaveBooking() {
		Iterable<Booking> begin= bookingRepository.findAll();
		
		Item i1= new Item("abc", "abc", 10, "cc", "adsds");
		List<Item> list1= new ArrayList<Item>();
		list1.add(i1);
		
		Booking b1= new Booking("Huiovcxvp", "fvcxvxccvcxvx", "cxv", "cxvxv");
		b1.setItems(list1);
		
		Item saved= itemRepository.save(i1);
		
		double price1=0;
		for (Item item : list1) {
			price1+= item.getPrice();
		}
		
		b1.setPrice(price1);
		bookingRepository.save(b1);
		
		Booking found= bookingRepository.save(b1);
		assertThat(found.toString()).isEqualTo(b1.toString());
		bookingRepository.delete(found);
		Iterable<Booking> end= bookingRepository.findAll();
		assertEquals((long) IterableUtils.size(begin), (long) IterableUtils.size(end));
		
	}
	
	@WithMockUser("USER")
	@Test
	// Testcase 3, test get list methods
	public void getList() throws  Exception {
		
		//Test get list of booking
		String uri_bookings = "/bookings";
		MvcResult mvcResult_booking = mvc.perform(MockMvcRequestBuilders.get(uri_bookings)
		    .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
		   
		int status_bookings = mvcResult_booking.getResponse().getStatus();
		assertEquals(200, status_bookings);
		String content_bookings = mvcResult_booking.getResponse().getContentAsString();
		BookingDTO [] bookinglist= super.mapFromJson(content_bookings, BookingDTO [].class);
		   assertTrue(bookinglist.length > 0);
		   
		 //Test get list of items
	    String uri_items = "/items";
		MvcResult mvcResult_item = mvc.perform(MockMvcRequestBuilders.get(uri_items)
		    .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
		   
		int status_items = mvcResult_item.getResponse().getStatus();
		assertEquals(200, status_items);
		String content_items = mvcResult_item.getResponse().getContentAsString();
		ItemDTO [] itemlist= super.mapFromJson(content_items, ItemDTO [].class);
		   assertTrue(itemlist.length > 0);
	}
	
	@WithMockUser("USER")
	@Test
	// TTestcase4, test get method by id, the respone should be 200 
	public void testById() throws  Exception {
		
		//Test get list of booking
		String uri_bookings = "/booking/1" ;
		MvcResult mvcResult_booking = mvc.perform(MockMvcRequestBuilders.get(uri_bookings)
		    .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
		   
		int status_bookings = mvcResult_booking.getResponse().getStatus();
		assertEquals(200, status_bookings);
		
		 //Test get list of items
	    String uri_items = "/item/1" ;
		MvcResult mvcResult_item = mvc.perform(MockMvcRequestBuilders.get(uri_items)
		    .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
		   
		int status_items = mvcResult_item.getResponse().getStatus();
		assertEquals(200, status_items);
	}
	
	@WithMockUser("USER")
	@Test
	// testcase5 post method, the respone should be 200 
	public void testPOST() throws  Exception {
		
		ItemDTO i=new ItemDTO(3,"sss", "ccxcx", "cxcxcx", 10, "cxcxcx");
		List<ItemDTO> list= new ArrayList<ItemDTO>();
		list.add(i);
		
		
		 //Test get list of items
	    String uri_items = "/item" ;
	    String inputJson_item = super.mapToJson(i);
	    MvcResult mvcResult_item = mvc.perform(MockMvcRequestBuilders.post(uri_items)
			      .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson_item)).andReturn();
		   
		int status_items = mvcResult_item.getResponse().getStatus();
		assertEquals(200, status_items);
		
	}
	
	@WithMockUser("USER")
	@Test
	// testcase6 post method, the respone should be 200 
	public void testPUT() throws  Exception {
		
		ItemDTO i=new ItemDTO(1,"sss", "ccxcx", "cxcxcx", 10, "cxcxcx");

		
		
		 //Test get list of items
	    String uri_items = "/item" ;
	    String inputJson_item = super.mapToJson(i);
	    MvcResult mvcResult_item = mvc.perform(MockMvcRequestBuilders.put(uri_items)
			      .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson_item)).andReturn();
		   
		int status_items = mvcResult_item.getResponse().getStatus();
		assertEquals(200, status_items);
		
	}
	
	@WithMockUser("USER")
	@Test
	// TTestcase4, test get method by id, the respone should be 200 
	public void testDelete() throws  Exception {
		
		//Test get list of booking
		String uri_bookings = "/booking/1" ;
		MvcResult mvcResult_booking = mvc.perform(MockMvcRequestBuilders.delete(uri_bookings)
		    .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
		   
		int status_bookings = mvcResult_booking.getResponse().getStatus();
		assertEquals(200, status_bookings);
		
		 //Test get list of items
	    String uri_items = "/item/1" ;
		MvcResult mvcResult_item = mvc.perform(MockMvcRequestBuilders.delete(uri_items)
		    .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
		   
		int status_items = mvcResult_item.getResponse().getStatus();
		assertEquals(200, status_items);
	}

}
