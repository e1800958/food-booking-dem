package fi.vamk.e1800957.foodbooking.Controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import fi.vamk.e1800957.foodbooking.DTO.ItemDTO;
import fi.vamk.e1800957.foodbooking.Entity.Booking;
import fi.vamk.e1800957.foodbooking.Entity.Item;
import fi.vamk.e1800957.foodbooking.Repository.BookingRepository;
import fi.vamk.e1800957.foodbooking.Repository.ItemRepository;

@RestController
public class ItemController {

	@Autowired
	private ItemRepository itemRepository;
	
	@Autowired
	private BookingRepository bookingRepository;
	
	@GetMapping("/items")
	public Iterable<ItemDTO> listByBooking(){
		try {
			ArrayList<ItemDTO> items= new ArrayList<ItemDTO>();
			for (Item item : itemRepository.findAll()) {
				items.add(item.convert());
			}
			return items;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
	}
	
	@GetMapping("/item/{id}")
	public ItemDTO itemById(@PathVariable("id") int id) {
		try {
			return itemRepository.findById(id).get().convert();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	@PostMapping("/item")
	public @ResponseBody ItemDTO newItem(@RequestBody ItemDTO item) {
		try {
			return itemRepository.save(item.convert()).convert();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
	}
	
	@PutMapping("/item")
	public @ResponseBody ItemDTO updateItem(@RequestBody ItemDTO item) {
		try {
			
			Item i2= item.convert();
			i2.setId(item.getId());
			return itemRepository.save(i2).convert();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
	}
	
	@DeleteMapping("/item/{id}")
	public void delete(@PathVariable("id") int id) {
		try {
			itemRepository.deleteById(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
