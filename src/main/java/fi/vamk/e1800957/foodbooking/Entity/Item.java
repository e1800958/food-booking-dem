package fi.vamk.e1800957.foodbooking.Entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.CascadeType;

import fi.vamk.e1800957.foodbooking.DTO.ItemDTO;

@Entity
@Table(name="Item")
//@NamedQuery(name = "Item.findAll", query = "SELECT i FROM Item i")
public class Item implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String title;
	private String description;
	private double price;
	private String image;
	private String status;
	
	@ManyToMany(mappedBy = "items")
	private List<Booking> bookings;
	
	
	public Item() {
		super();
	}
	public Item(String title, String description, double price, String status, String image) {
		super();
		this.title = title;
		this.description = description;
		this.price = price;
		this.status=status;
		this.image=image;
		
	}
	
	public Item(String title, String description, double price, String status,String image, List<Booking> bookings) {
		super();
		this.title = title;
		this.description = description;
		this.price = price;
		this.status = status;
		this.bookings = bookings;
		this.image=image;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public int getId() {
		return id;
	}
	
	
	
//	public Booking getBooking_id() {
//		return booking_id;
//	}
//	public void setBooking_id(Booking booking_id) {
//		this.booking_id = booking_id;
//	}
	
	
	
	
	public void setId(int id) {
		this.id = id;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public List<Booking> getBookings() {
		return bookings;
	}
	public void setBookings(List<Booking> bookings) {
		this.bookings = bookings;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public ItemDTO convert() {
		ItemDTO i = new ItemDTO();
		i.setId(this.id);
		i.setTitle(this.title);
		i.setDescription(this.description);
		i.setPrice(this.price);
		i.setStatus(this.status);
		i.setImage(this.image);
		return i;
	}
	@Override
	public String toString() {
		return "Item [id=" + id + ", title=" + title + ", description=" + description + ", price=" + price + "]";
	}
	
	
	
	

}
